# Pokedex for Sqreen

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It aims to display all Pokemon and allows for filtering and search capabilities.

## Run the project

First, you need to clone this repository.
Then, go to the project directory and run:

```
npm install // Install all dependencies
npm start // Run the app in development mode
```

The app will be available on [http://localhost:3000](http://localhost:3000).

## Search and filtering system

By default, all pokemon will be displayed.

You can search pokemon by name or type using the main search text input.

You can also filter the pokemon by heights (3 height categories are available) or weaknesses.

## Browsers support

This app has been tested on Chrome, Firefox and Edge. Since I don't have an Apple device, I was not able to test it on Safari. It does not support Internet Explorer (but I can add polyfills if needed).

It is responsive.

## What could be improved?

- We could do a more user-friendly search input. When you start typing in the search input, a dropdown could be displayed with 2 categories: pokemon names matching the search value and pokemon types. When you select an item in the dropdown, it applies the corresponding filter (name or type). The values matching search could be highlighted for each pokemon.
- Pagination system
- Add Router dependency to use query strings. When a filter is selected (height or weakness) or search input changes, it could update query strings. This way, we could easily share pokemon results.
