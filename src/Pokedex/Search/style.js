import styled from 'styled-components';
import { TextField } from '@material-ui/core';
import theme from '../../constants/theme';

const SearchContainer = styled.div`
  width: 100%;
  padding: 2rem 2rem 1rem;
  display: flex;
  justify-content: center;
`;

const SearchInput = styled(TextField)`
	width: 80%;
	@media (${theme.breakpoints.mobile.type}-width:
    ${theme.breakpoints.mobile.value}px) {
    width: 100%;
  }
`;

export { SearchContainer, SearchInput };
