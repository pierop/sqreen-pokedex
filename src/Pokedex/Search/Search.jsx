import React, { Component } from 'react';
import { SearchContainer, SearchInput } from './style';

class Search extends Component {
  onChangeHandler = event => {
    const { onSearchChange } = this.props;
    if (!!onSearchChange) {
      onSearchChange(event.target.value);
    }
  };

  render() {
    const { value } = this.props;
    return (
      <SearchContainer>
        <SearchInput
          label="Search a pokemon by name or type"
          value={value}
          onChange={this.onChangeHandler}
          variant="outlined"
        />
      </SearchContainer>
    );
  }
}

export default Search;
