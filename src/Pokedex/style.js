import styled from 'styled-components';
import theme from '../constants/theme';
import FlexContainer from '../wrappers/FlexContainer';

const PokedexContainer = styled.div`
  background-color: ${theme.colors.light};
  min-height: 100vh;
`;

const FiltersContainer = styled(FlexContainer)`
  padding: 1rem 2rem;
`;

export { PokedexContainer, FiltersContainer };
