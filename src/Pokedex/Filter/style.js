import { FormControl } from '@material-ui/core';
import styled from 'styled-components';
import theme from '../../constants/theme';

const FilterWrapper = styled(FormControl)`
  width: 40%;
  @media (${theme.breakpoints.mobile.type}-width:
    ${theme.breakpoints.mobile.value}px) {
    width: 100%;
  }
`;

export { FilterWrapper };
