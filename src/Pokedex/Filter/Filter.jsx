import {
  InputLabel,
  MenuItem,
  Select,
  Checkbox,
  ListItemText,
  Chip
} from '@material-ui/core';
import Input from '@material-ui/core/Input';
import React, { Component } from 'react';
import { FilterWrapper } from './style';

class Filter extends Component {
  handleChange = event => {
    const { onSelectChoices } = this.props;
    const { value } = event.target;

    if (!!onSelectChoices) {
      onSelectChoices(value);
    }
  };

  render() {
    const { name, label, choices, selectedChoices } = this.props;

    return (
      <FilterWrapper>
        <InputLabel htmlFor={`select-${name}`}>{label}</InputLabel>
        <Select
          multiple
          value={selectedChoices}
          onChange={this.handleChange}
          input={<Input id={`select-${name}`} />}
          renderValue={selectedChoices => (
            <div>
              {selectedChoices.map(choice => (
                <Chip key={choice} label={choice} />
              ))}
            </div>
          )}
        >
          {choices.map(choice => {
            const isSelected = selectedChoices.includes(choice);
            return (
              <MenuItem key={choice} value={choice}>
                <Checkbox checked={isSelected} />
                <ListItemText primary={choice} />
              </MenuItem>
            );
          })}
        </Select>
      </FilterWrapper>
    );
  }
}

export default Filter;
