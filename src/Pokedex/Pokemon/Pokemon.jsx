import Chip from '@material-ui/core/Chip';
import React, { Component } from 'react';
import FlexContainer from '../../wrappers/FlexContainer';
import { Label, PokemonContainer, PokemonInfo, PokemonName } from './style';

class Pokemon extends Component {
  render() {
    const { pokemon } = this.props;
    return (
      <PokemonContainer>
        <img alt={pokemon.name} src={pokemon.image} />
        <PokemonInfo>
          <FlexContainer>
            <PokemonName>{pokemon.name}</PokemonName>
            <div>
              <Label>Type</Label>
              {pokemon.types.map((type, index) => {
                return <Chip key={`type-${index}`} label={type} />;
              })}
            </div>
          </FlexContainer>
          <FlexContainer>
            <span>
              <Label>Height</Label>
              <span>{pokemon.height}</span>
            </span>
            <span>
              <Label>Weaknesses</Label>
              {pokemon.weaknesses.map((weakness, index) => {
                return <Chip key={`type-${index}`} label={weakness} />;
              })}
            </span>
          </FlexContainer>
        </PokemonInfo>
      </PokemonContainer>
    );
  }
}

export default Pokemon;
