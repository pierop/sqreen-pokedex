import styled from 'styled-components';
import theme from '../../constants/theme';
import FlexContainer from '../../wrappers/FlexContainer';

const PokemonContainer = styled(FlexContainer)`
  margin: 1rem;
  padding: 1rem;
  background-color: white;
  border-radius: 5px;
  box-shadow: 0 5px 15px 0 rgba(37, 44, 97, 0.15),
    0 2px 4px 0 rgba(93, 100, 148, 0.2);
`;

const PokemonInfo = styled.div`
  flex: 1 1 auto;
  
  @media (${theme.breakpoints.tablet.type}-width:
    ${theme.breakpoints.tablet.value}px) {
    margin-left: 1rem !important;
  }
`;

const PokemonName = styled.h2`
  color: ${theme.colors.primary};
`;

const Label = styled.h3`
  display: inline-block;
  margin-right: 1rem;
`;

export { PokemonContainer, PokemonInfo, PokemonName, Label };
