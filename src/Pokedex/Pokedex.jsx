import React, { Component } from 'react';
import Filter from './Filter/Filter';
import Pokemon from './Pokemon/Pokemon';
import Search from './Search/Search';
import { FiltersContainer, PokedexContainer } from './style';

const HEIGHT_CATEGORIES = [
  { min: 0, max: 1, label: 'Less than 1m' },
  { min: 1, max: 2, label: 'Between 1m and 2m' },
  { min: 2, max: 1000, label: 'More than 2m' }
];

class Pokedex extends Component {
  state = {
    pokemon: [],
    filteredPokemon: [],
    search: '',
    heightFilters: [],
    heights: HEIGHT_CATEGORIES.map(h => h.label),
    weaknessFilters: [],
    weaknesses: []
  };

  componentDidMount() {
    fetch(
      'https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json'
    )
      .then(result => result.json())
      .then(result => {
        const data = result.pokemon;
        const pokemon = data.map(pokemon => {
          return {
            id: pokemon.id,
            name: pokemon.name,
            image: pokemon.img,
            types: pokemon.type,
            height: pokemon.height,
            weaknesses: pokemon.weaknesses
          };
        });

        // Extract list of available weaknesses
        let weaknesses = [];
        pokemon.forEach(p => {
          p.weaknesses.forEach(weakness => {
            if (!weaknesses.includes(weakness)) {
              weaknesses.push(weakness);
            }
          });
        });
        this.setState({ pokemon, weaknesses: weaknesses.sort() }, () => {
          this.filterPokemon();
        });
      });
  }

  isMatchingSearch(pokemon) {
    const { search } = this.state;

    const hasMatchingName = pokemon.name
      .toLowerCase()
      .startsWith(search.toLowerCase());
    const hasMatchingType = pokemon.types.some(t =>
      t.toLowerCase().startsWith(search.toLowerCase())
    );
    return hasMatchingName || hasMatchingType;
  }

  isMatchingHeightFilters(pokemon) {
    const { heightFilters } = this.state;
    const height = pokemon.height.split(' ')[0];
    const selectedHeightCategories = heightFilters.map(filter => {
      return HEIGHT_CATEGORIES.find(category => category.label === filter);
    });

    let isMatchingHeightFilters = true;
    if (!!selectedHeightCategories && !!selectedHeightCategories.length) {
      isMatchingHeightFilters = selectedHeightCategories.some(category => {
        return height >= category.min && height < category.max;
      });
    }
    return isMatchingHeightFilters;
  }

  isMatchingWeaknessFilters(pokemon) {
    const { weaknessFilters } = this.state;

    if (!!weaknessFilters && !!weaknessFilters.length) {
      return pokemon.weaknesses.some(weakness => {
        return weaknessFilters.includes(weakness);
      });
    }
    return true;
  }

  filterPokemon() {
    const { pokemon } = this.state;
    const filteredPokemon = pokemon.filter(p => {
      const isMatchingSearch = this.isMatchingSearch(p);
      const isMatchingHeightFilters = this.isMatchingHeightFilters(p);
      const isMatchingWeaknessFilters = this.isMatchingWeaknessFilters(p);

      return (
        isMatchingSearch && isMatchingHeightFilters && isMatchingWeaknessFilters
      );
    });
    this.setState({ filteredPokemon });
  }

  searchChangeHandler = search => {
    this.setState({ search }, () => {
      this.filterPokemon();
    });
  };

  heightFiltersChangeHandler = heightFilters => {
    this.setState({ heightFilters }, () => {
      this.filterPokemon();
    });
  };

  weaknessFiltersChangeHandler = weaknessFilters => {
    this.setState({ weaknessFilters }, () => {
      this.filterPokemon();
    });
  };

  render() {
    const {
      filteredPokemon,
      search,
      heightFilters,
      heights,
      weaknessFilters,
      weaknesses
    } = this.state;
    return (
      <PokedexContainer>
        <Search value={search} onSearchChange={this.searchChangeHandler} />
        <FiltersContainer>
          <Filter
            name="height-filter"
            label="Filter by height"
            choices={heights}
            selectedChoices={heightFilters}
            onSelectChoices={this.heightFiltersChangeHandler}
          />
          <Filter
            name="weakness-filter"
            label="Filter by weakness"
            choices={weaknesses}
            selectedChoices={weaknessFilters}
            onSelectChoices={this.weaknessFiltersChangeHandler}
          />
        </FiltersContainer>
        {filteredPokemon.map(pokemon => {
          return <Pokemon key={`pokemon-${pokemon.id}`} pokemon={pokemon} />;
        })}
      </PokedexContainer>
    );
  }
}

export default Pokedex;
