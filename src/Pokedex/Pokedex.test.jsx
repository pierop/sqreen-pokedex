import React from 'react';
import { mount, shallow } from 'enzyme';
import Pokedex from './Pokedex';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

// Mock API call
function mockFetch() {
  return jest.fn().mockImplementation(() =>
    Promise.resolve({
      ok: true,
      json: () => {
        return {
          pokemon: [
            {
              id: 1,
              name: 'Bulbasaur',
              img: 'http://www.serebii.net/pokemongo/pokemon/001.png',
              type: ['Grass', 'Poison'],
              height: '0.71 m',
              weaknesses: ['Fire', 'Ice', 'Flying', 'Psychic']
            },
            {
              id: 2,
              name: 'Ivysaur',
              img: 'http://www.serebii.net/pokemongo/pokemon/002.png',
              type: ['Grass', 'Poison'],
              height: '0.99 m',
              weaknesses: ['Fire', 'Ice', 'Flying', 'Psychic']
            }
          ]
        };
      }
    })
  );
}

describe('Pokedex', () => {
  describe('componentDidMount', () => {
    it('should call api using fetch', async () => {
      fetch = mockFetch();

      const fetchSpy = jest.spyOn(window, 'fetch');
      shallow(<Pokedex />);
      expect(fetchSpy).toBeCalled();
    });
    it('should initialize state after fetching pokemon', async () => {
      fetch = mockFetch();
      const mounted = mount(<Pokedex />);

      return Promise.resolve(mounted)
        .then(() => mounted.update())
        .then(() => {
          expect(mounted.state('pokemon').length).toBe(2);
          expect(mounted.state('filteredPokemon').length).toBe(2);
        });
    });
  });

  describe('filterPokemon', () => {
    it('should update filtered pokemon according to search', async () => {
      fetch = mockFetch();
      const mounted = mount(<Pokedex />);

      return Promise.resolve(mounted)
        .then(() => mounted.update())
        .then(() => {
          mounted.setState({ search: 'bul' }, () => {});
          mounted.instance().filterPokemon();
          expect(mounted.state('pokemon').length).toBe(2);
          expect(mounted.state('filteredPokemon').length).toBe(1);
        });
    });
  });
});
